# C'est quoi ?

Module IDE - GIT - POO IMIE Janvier 2018.  
Animé par Benoit Juin - société [AEon Création](https://www.aeon-creation.com)

# C'est pour qui ?

Pour ceux qui veulent devenir développeur et tant qu'a faire:

- autonome,
- confiant en son code,
- et capable de travailler un code avec d'autres.

# Questions

- vous connaissez quoi en programmation ?
- avez vous déjà utilisé un autre langage que python ?
- avez vous déjà coder quelque chose ?
- pourquoi vous voulez apprendre à coder ?
- vous utilisez: windows, linux, mac, autre ?

# Plan du cours

## IDE

1. Qu-est ce qu'un IDE
2. Quelques IDE courants
3. Un IDE pour python
4. Bonnes pratiques


## GIT

1. Gérez vos codes source avec Git
  * Qu'est-ce qu’un logiciel de gestion de versions ?
  * Logiciels centralisés et distribués
2. Git et les autres logiciels de gestion de versions
  * Les différents logiciels de gestion de versions
  * Quelles sont les particularités de Git ?
3. Installer et configurer Git
  * Installation
  * Configurer Git
4. Créer un nouveau dépôt ou cloner un dépôt existant
  * Créer un nouveau dépôt
  * Cloner un dépôt existant
  * Créer un dépôt qui servira de serveur
5. Modifier le code et effectuer des commits
  * Méthode de travail
  * Effectuer un commit des changements
6. Annuler un commit effectué par erreur
  * Que s’est-il passé ? Vérifions les logs
  * Corriger une erreur
7. Télécharger les nouveautés et partager votre travail
  * Télécharger les nouveautés
  * Envoyer vos commits
  * Annuler un commit publié
8. Travailler avec des branches
  * Les branches locales
  * Les branches partagées
9. Quelques autres fonctionnalités de Git, en vrac
  * Tagger une version
  * Rechercher dans les fichiers source
  * Demander à Git d’ignorer des fichiers (.gitignore)
  * Partager

## POO

1. Les classes
2. Les propriétés
3. Les méthodes spéciales
4. Le tri ?
5. L'héritage
6. Décorateurs & Méta-classes


# Support de cours

 * git: https://gitlab.com/imie-ide-git-poo-janvier-2018/cours/raw/develop/git.pdf
 * python: https://gitlab.com/imie-ide-git-poo-janvier-2018/cours/raw/develop/python.pdf